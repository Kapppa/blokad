#!/bin/bash

VERSION="1.0.0"

echo "Converts an OISD blocklist (https://oisd.nl) to Blokad format"

if [ -z "$1" ];
then
    echo "You must specify the source OISD blocklist file"
    exit
fi

if [ -z "$2" ];
then
    echo "You must specify the output Blokad blocklist file"
    exit
fi

echo "# Blokad converted OISD domain wildcards blocklist" > $2
echo "# $(date)" >> $2
echo "#" >> $2

cat $1 | grep "^*" >/tmp/blokad.list
sed 's/^/0.0.0.0 /' /tmp/blokad.list > /tmp/blokad.list.converted
rm /tmp/blokad.list

cat /tmp/blokad.list.converted >> $2
rm /tmp/blokad.list.converted

echo "Conversion finished"