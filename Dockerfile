FROM debian

RUN apt update && apt install -y build-essential wget

COPY src /src

RUN cd /src && make && make install

COPY utils/convertOISD.sh /
COPY entrypoint.sh /


ENTRYPOINT ["/entrypoint.sh"]

