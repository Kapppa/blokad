#!/bin/bash

if [ ! -f /etc/hosts.blokad.blocklist ]
then
	wget https://big.oisd.nl/domainswild -O /tmp/blocklist && /convertOISD.sh /tmp/blocklist /etc/hosts.blokad.blocklist
fi

/usr/sbin/blokad -b /etc/hosts.blokad.blocklist -l

 
