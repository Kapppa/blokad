Blokad
========

A tiny DNS ad blocker

## Compiling & Installation

```bash
$ git clone https://gitlab.com/Kapppa/blokad.git
$ cd blokad
$ make && make install
```

## Simple tutorial

```bash
$ blokad -h
Usage: blokad [options]
  -d or --daemon
                       (daemon mode)
  -p <port> or --port=<port>
                       (local bind port, default 53)
  -R <ip> or --remote-addr=<ip>
                       (remote server ip, default AdGuard DNS [94.140.14.14])
  -P <port> or --remote-port=<port>
                       (remote server port, default 53)
  -T or --remote-tcp
                       (connect remote server in tcp, default no)
  -b <file> or --block-file=<file>
                       (Domains to block)
  -o <file> or --hosts-file=<file>
                       (Your own host file)
  -l --log-queries
                       (Log queries and results)
  -h, --help           
                       (print help and exit)
  -v, --version        
                       (print version and exit)
```

## Example hosts blocklist file example

```
0.0.0.0 *.test.com
0.0.0.0 2*.test.com
0.0.0.0 *3.test.com
```

Based on [dnsproxy](https://github.com/vietor/dnsproxy.git)

