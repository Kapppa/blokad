/*
 * Copyright 2014, Vietor Liu <vietor.liu at gmail.com>
 * 
 * Turned it into Blokad by Ka
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version. For full terms that can be
 * found in the LICENSE file.
 */

char *ascii_logo =
	"                                \n"
	"  ____  _       _             _ \n"
	" | __ )| | ___ | | ____ _  __| |\n"
	" |  _ \\| |/ _ \\| |/ / _` |/ _` |\n"
	" | |_) | | (_) |   < (_| | (_| |\n"
	" |____/|_|\\___/|_|\\_\\__,_|\\__,_|\n"
	"                                \n";
